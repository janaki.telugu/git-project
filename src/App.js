import './App.css';
import {BrowserRouter ,Routes,Route} from 'react-router-dom'
import Header from './Component/Header';
import { Provider } from './Component/Create';
import Home from './Component/Home';
import Laptops from './Component/Laptops';
import Mobiles from './Component/Mobiles';
import Accessories from './Component/Accessories';
import Cart from './Component/Cart';
import Private from './Component/Private';


function App() {
  return (
    <div className="App">
      <Provider>
      <BrowserRouter>
        <Header/>
        <Routes>
        <Route path='/' element={<Home/>}/>
          <Route path='/home' element={<Home/>}/>
           <Route path='/laptops' element={<Laptops />} />
           <Route path='/mobiles' element={<Mobiles/>}/>
           <Route path='/accessories' element={<Accessories />} />
           <Route element={<Private isLogin={true}/>}>
           <Route path='/cart' element={<Cart/>} />
            
           </Route>

        </Routes>
      </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
