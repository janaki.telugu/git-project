import React, { createContext, useState } from 'react'


export const Create = createContext();

export const Provider = (props) => {

    const [list, setList] = useState([
        
        {

             
            "imageurl":"https://purepng.com/public/uploads/large/purepng.com-laptopelectronicslaptopcomputer-941524676379zuw2c.png",
            "name":"HP-Black",
            "category": "Laptop",
            "price":60000,
        },
        {

             
            "imageurl":"https://m.media-amazon.com/images/I/71MHTD3uL4L.jpg",
            "name":"Apple iPhone 12 Pro",
            "category": "Mobile",
            "price":40000,
        },
        {

             
            "imageurl":"https://img.joomcdn.net/758d0c84c78719cdc26a67e5ce3bf749555ef5a6_original.jpeg",
            "name":"HP Bluetooth Headset ",
            "category": "Accesories",
            "price":1000,
        },
        
       
      
        
      
        
        {

             
            "imageurl":"https://www.pngfind.com/pngs/m/658-6586213_alienware-18-dell-inspiron-15-5000-gaming-laptop.png",
            "name":"dell-inspiron-5000-gaming-laptop",
            "category": "Laptop",
            "price":58000,
        },
        {

             
            "imageurl":"https://cdn.imgbin.com/7/25/1/imgbin-macbook-pro-15-4-inch-apple-mac-mini-laptop-macbook-HsSHaV8t1MH1uY87xMwCGSrw4.jpg",
            "name":"macbook-pro-15-4-inch-apple-mac-mini-laptop",
            "category": "Laptop",
            "price":49000,
        },
        {

             
            "imageurl":"https://www.pngfind.com/pngs/m/677-6777181_hp-i3-laptop-silver-hd-png-download.png",
            "name":"hp-i3-laptop-silver",
            "category": "Laptop",
            "price":55000,
        },
       
        {

          
            "imageurl":"https://chromeunboxed.com/wp-content/uploads/2021/08/Untitled-1-2-1200x900.jpg",
            "name":"lenovo",
            "category": "Laptop",
            "price":55000,
        },
        {

        
            "imageurl":"https://static.toiimg.com/photo/msid-78642800/78642800.jpg",
            "name":"Apple iPhone 12",
            "category": "Mobile",
            "price":28000,
        },
        {

         
            "imageurl":"https://cdn.bajajelectronics.com/product/7375-1.png",
            "name":"Redmi Note 10S",
            "category": "Mobile",
            "price":32000,
        },
        {

       
            "imageurl":"https://static.techspot.com/images/products/2019/smartphones/org/2020-04-14-product-3.png",
            "name":"OnePlus 8",
            "category": "Mobile",
            "price":40000,
        },
        
        {

          
            "imageurl":"https://www.citypng.com/public/uploads/preview/-11599003756hnkmfnfuhg.png",
            "name":"samsung",
            "category": "Mobile",
            "price":35000,
        },
        
        {

        
            "imageurl":"https://www.vhv.rs/dpng/d/572-5725669_realme-png-image-realme-2-pro-4gb-ram.png",
            "name":"realme",
            "category": "Mobile",
            "price":15000,
        },
        
      
        {

        
            "imageurl":"https://e7.pngegg.com/pngimages/661/870/png-clipart-lenovo-vibe-k5-plus-lenovo-vibe-p1-lenovo-smartphones-android-android-gadget-mobile-phone.png",
            "name":"lenovo",
            "category": "Mobile",
            "price":10000,
        },
       
        {

          
            "imageurl":"https://www.pngitem.com/pimgs/m/508-5084452_phone-accessories-battery-charger-hd-png-download.png",
            "name":"battery charger ",
            "category": "Accesories",
            "price":600,
        },
        {

         
            "imageurl":"https://sc04.alicdn.com/kf/H439f3a340eb44f91809684dfa0f8c5d4o.png",
            "name":"Bluetooth earphones ",
            "category": "Accesories",
            "price":1300,
        },
        {

        
            "imageurl":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJB8rrurUBLZZJAOICp49Zti5Htpc6YOVpAw&usqp=CAU",
            "name":"camera ",
            "category": "Accesories",
            "price":30000,
        },
        
        {

           
            "imageurl":"https://cdn.imgbin.com/6/23/22/imgbin-headset-bluetooth-mobile-phones-wireless-headphones-bluetooth-ZAqY16pdjtrcfvEauhc63yPRR.jpg",
            "name":"wireless bluetooth ",
            "category": "Accesories",
            "price":1000,
        },
      
        
       
        {

             
            "imageurl":"https://pngimg.com/uploads/printer/printer_PNG7756.png",
            "name":"printer ",
            "category": "Accesories",
            "price":10,
        },
        {

             
            "imageurl":"https://www.pngall.com/wp-content/uploads/5/Computer-Accessories-PNG-Image-File.png",
            "name":"Table ",
            "category": "Accesories",
            "price":849,
        },
        
        
       
       
       
        {

           
            "imageurl":"https://www.pngitem.com/pimgs/m/297-2978265_dell-laptop-png-images-dell-xps-15-9570.png",
            "name":"dell-xps-15-8gb ",
            "category": "Laptop",
            "price":48000,
        },
        {

            
            "imageurl":"https://e7.pngegg.com/pngimages/235/457/png-clipart-vaio-laptop-windows-7-device-driver-windows-vista-sony-laptop-computers-netbook-computer.png",
            "name":"sony ",
            "category": "Laptop",
            "price":45000,
        },
       
       

    ])

    return (
        <Create.Provider value={[list,setList]}>
            {props.children}
        </Create.Provider>
    )
}

