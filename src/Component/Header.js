import { Grid } from '@mui/material'
import React from 'react'
import{Link} from 'react-router-dom'

function Header() {
  return (
    <Grid container>
       <Grid item lg={12} md={12} sm={12} xs={12} sx={{width:"100%",height:80,backgroundColor:"blue",display:"flex",
       justifyContent:"space-around",paddingTop:3,}}>
       <Link style={{color:"white",fontSize:25,fontWeight:900,fontStyle:"italic",textDecoration:"none"}} to="/home"><i class="fa-solid fa-house"></i>Home</Link>
       <Link style={{color:"white",fontSize:25,fontWeight:900,fontStyle:"italic",textDecoration:"none"}} to="/laptops"><i class="fa-solid fa-laptop"></i>Laptops</Link>
       <Link style={{color:"white",fontSize:25,fontWeight:900,fontStyle:"italic",textDecoration:"none"}} to="/mobiles"><i class="fa-solid fa-mobile-retro"></i>Mobiles</Link>
       <Link style={{color:"white",fontSize:25,fontWeight:900,fontStyle:"italic",textDecoration:"none"}} to="/accessories"><i class="fa-solid fa-headphones"></i>Accessories</Link>
       <Link style={{color:"white",fontSize:25,fontWeight:900,fontStyle:"italic",textDecoration:"none"}} to="/cart"><i class="fa-solid fa-cart-shopping"></i>Cart</Link>

     </Grid> 

    </Grid>
  )
}

export default Header