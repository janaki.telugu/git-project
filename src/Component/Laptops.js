import { Button, Grid, Typography } from '@mui/material';
import { useContext } from 'react'
import { Create } from "./Create";
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';

 function Laptops() {
    
    const [list] = useContext(Create);

    const product = list.filter((data) => data.category === "Laptop")

return (
        
    <Grid container>
    {
    product.map((items) => {
     return (
 <Grid item lg={4} md={6} sm={12} xs={12} sx={{paddingTop:10,paddingLeft:10}} >
                              
<Card sx={{ maxWidth: 345,height:"500px" }}>
      <CardMedia
        component="img"
    //    sx={{width:300,height:200}}
    sx={{maxWidth:"100%",height:"auto",position:"relative"}}

        image={items.imageurl}
        alt="green iguana"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" sx={{color:"red"}} component="div">
         {items.name}
        </Typography>
        <Typography variant="h6" sx={{color:"black"}}>
         Price:$ {items.price}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="big" variant='contained' sx={{marginLeft:12}}>Add to Cart</Button>
      </CardActions>
    </Card>
 </Grid>
    )
    })
    }
    </Grid>
    )
}
export default Laptops