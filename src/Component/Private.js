import { Navigate,Outlet } from "react-router-dom";

function Private({isLogin}){
  return isLogin?<Outlet/>:<Navigate to="/"/>
}
export default Private